# 백업 스크립트 By CONORY var 0.1
 
# 백업설정
TODAY=`date +%Y%m%d` 
DELETE_DATE=`date -d "-2 days" +%Y%m%d` 
BACKUP_DIR=/var/backup
LOG_DIR=/var/log/backup
 
#필요한 디렉토리 생성
if [ ! -d ${BACKUP_DIR} ]
then
    mkdir ${BACKUP_DIR}
    chmod 700 ${BACKUP_DIR}
fi
if [ ! -d ${LOG_DIR} ]
then
    mkdir ${LOG_DIR}
fi
 
# 홈 디렉토리 백업
TGZ_DIR="/home"
TGZ_FILE="backup.tar.gz"
tar cfz ${BACKUP_DIR}/${TODAY}_${TGZ_FILE} ${TGZ_DIR}
 
# DB Root 계정정보
DB_USER="root"
DB_PW="didrPdls2019!"
 
#전체 DB 백업
SQL_FILE="DBbackup.sql"
mysqldump -u ${DB_USER} -p${DB_PW} --all-databases > ${BACKUP_DIR}/${TODAY}_${SQL_FILE}
 
# 백업서버 FTP 계정정보
#FTP_HOST="백업본을 전송할 원격서버 주소(ip나 도메인)"
#FTP_USER="FTP 계정아이디"
#FTP_PW="FTP 계정비밀번호"
 
# 원격 백업서버로 백업파일 업로드 (업로드에 실패하면 재시도)
LOG_FILE=${LOG_DIR}/${TODAY}.log
#while [ ! -e $LOG_FILE ] || [ `egrep -c "226 Transfer complete" ${LOG_FILE}` -lt 2 ]
#do
#    {
#    echo user $FTP_USER $FTP_PW
#    echo bi
#    echo prompt
#    echo lcd ${BACKUP_DIR}
#    echo mdelete ${DELETE_DATE}_${TGZ_FILE} ${DELETE_DATE}_${SQL_FILE}
#    echo mput ${TODAY}_${TGZ_FILE} ${TODAY}_${SQL_FILE}
#    echo bye
#    } | ftp -n -v $FTP_HOST > $LOG_FILE
#done
 
# 로컬 백업파일 삭제
#rm -f ${BACKUP_DIR}/${TODAY}_${TGZ_FILE} ${BACKUP_DIR}/${TODAY}_${SQL_FILE}
# 오래된 백업데이터 삭제(3일 이상 된 것)
find ${BACKUP_DIR}/ -mtime +3 -exec rm -f {} \;


echo mdelete ${DELETE_DATE}_${TGZ_FILE} ${DELETE_DATE}_${SQL_FILE}
