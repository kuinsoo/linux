### Mariadb Setting 
### Docker Image
> docker pull mariadb:latest
### Docker Run
> docker run -p 3306:3306 -v /home/kuinsoo/mariadb:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=0701 --restart="always" --name mariadb mariadb:latest

### Mariadb 진행
#### 접속
> mysql -uroot -p0701
#### 데이터베이스 리스트 확인
> show database;
#### 데이터베이스 생성
> create database DB명;
#### 기본 database 접속
> use mysql;
#### mysql의 user 테이블에서 이미 생성된 계정 확인
> select host, user, password from user;
#### mysql은 보안상 기본적으로 외부접속을 허용하지 않기 때문에 계정을 생성할때 특정 IP 혹은 localhost 를 지정하거나 %를 지정하여 외부접속을 허용할 수 있다.
#### 계정생성
> create user '계정아이디'@'접속위치' identified by '패스워드';
#### 권한 주기
> grant all privileges on DB이름.테이블 to '계정아이디'@'접속위치'; \
> 예>\
> grant all privileges on testDB.* to 'user1'@'localhost'; \
> grant select on testDB.* to 'user1'@'%';
#### 권한 확인 
> show grants for '계정아이디'@'접속위치'
#### 계정 삭제
> drop user '계정아이디'@'접속위치';
#### 권한 삭제
> revoke all on DB이름.테이블 from '계정아이디'@'접속위치';