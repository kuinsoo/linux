# Cron ? 
특정한 시간에 또는 특정 시간 마다 어떤 작업을 자동으로 수행하게 해주고 싶을때 사용하는 명령어가 cron 입니다.\
cron 은 특정한 시간에 특정한 작업을 수행하게 해주는 스케줄링 역활을 합니다. \
cron 스시템에는 시스템에서 기본적으로 사용하는 cron 설정이 있으며, 이를 시스템크론이라고 합니다. \
또 root 나 일반 사용자가 자신의 cron 설정을 직접 사용하는 사용자크론이 있습니다. 

# Cron 설치
```
$ apt-get install -y cron
```

# Cron 
>1. crontab 
cron 작업을 설정하는 파일을 crontab파일이라고 합니다.\
coron 프로세서는 /etc/crontab 파일에 설정된 것을 읽어서 작업을 수행합니다.\
crontab 파일은 OS 별로 각각 다른 위치에 저장이 됩니다.\

형식은 다음과 같이 총 7개의 필드로 구성되어 있습니다\
<span style="background:gray;">분 시 일 월 요일 사용자 실행명령 (일단 기억해두기)</span>\
25 6 * * * root test -x /usr/sbin/anacron ~ 

이런 줄들을 볼 수 있는데 여기서 *는 매월, 매일 할 때 그 every를 뜻 해요. 3번째 칸이 *이면 매일, 4번째 칸이 *이면 매월이겠죠?\
-e옵션으로 편집하고 저장 한 후 그냥 빠져나오면 크론설정이 끝이다.ㅎㅎ 


# 옵션
<pre>
~$ crontab -e

개별사용자의 cron 설정 방법 

~$ crontab -l

개별사용자의 cron 확인 방법 

~$ crontab -r

개별사용자의 cron 삭제 방법 

# crontab -u 사용자명 -e
root의 일반 사용자 cron 설정 방법
# crontab -e -u posein (posein 사용자의 crontab 내용을 작성하거나 수정한다~)
# crontab -u 사용자명 -l
root의 일반 사용자 cron 설정 확인 방법
# crontab -u 사용자명 -r
root의 일반 사용자 cron 설정 삭제 방법
</pre>

# 유용한 cron 설정 예시 
<pre>

1.  매일 2.am에 백업하기 

-> 0 2 * * * /bin/sh backup.sh  (참고로 추후 데이터베이스 파트에서 실제로 cron을 이용해서 자동 백업을 진행해볼거예요)

2. 하루에 두번 script.sh 수행

-> 0 5, 17 * * * /scripts/script.sh

3. 매분 수행 

-> * * * * * /scripts/script.sh

4. 매 10분마다 monitor 스크립트 실행

-> */10 * * * * /scripts/monitor.sh

5. 1월부터 12월까지 2개월마다 1일날 오전 4시 10분에 /etc/check.sh라는 스크립트 실행

-> 10 4 1 1-12/2 * /etc/check.sh

6. 금요일하고 일요일 5시에만 스크립트 수행되게 

-> 0 17 * * sun,fri /script/script.sh

7. 매달 첫 번째 일요일에만 시행되게

-> 0 2 * * sun [$(date + %d) -le 07] && /script/scripnt.sh

</pre>